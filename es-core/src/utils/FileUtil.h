//
// Created by bkg2k on 31/07/2019.
//

#ifndef EMULATIONSTATION_ALL_FILEUTIL_H
#define EMULATIONSTATION_ALL_FILEUTIL_H

#include <string>

class FileUtil
{
  public:
    static std::string loadTextFile       (const std::string& path);
};


#endif //EMULATIONSTATION_ALL_FILEUTIL_H
